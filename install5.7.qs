function Controller() {
 //gui.setSilent(true);
 installer.autoRejectMessageBoxes();
    installer.installationFinished.connect(function() {
        gui.clickButton(buttons.NextButton,3000);
    })
}

Controller.prototype.WelcomePageCallback = function() {
    gui.clickButton(buttons.NextButton, 3000);
}


Controller.prototype.CredentialsPageCallback = function() {
  //var page = gui.pageWidgetByObjectName("CredentialsPage");
  //console.log(gui)
  //var widget = gui.currentPageWidget();
  //widget.loginWidget.EmailLineEdit.setText("");
  //widget.loginWidget.PasswordLineEdit.setText("");
  gui.clickButton(buttons.NextButton, 1000);
}

Controller.prototype.IntroductionPageCallback = function() {
    gui.clickButton(buttons.NextButton, 3000);
}

Controller.prototype.TargetDirectoryPageCallback = function()
{
    gui.currentPageWidget().TargetDirectoryLineEdit.setText("C:/Qt/Qt5.7.1");
    gui.clickButton(buttons.NextButton, 3000);
}
Controller.prototype.ComponentSelectionPageCallback = function() {
    function list_packages() {
      var components = installer.components();
      console.log("Available components: " + components.length);
      var packages = ["Packages: "];
      for (var i = 0 ; i < components.length ;i++) {
         packages.push(components[i].name+"\n");
		  //console.log(components[i].name)
     }
	//console.log("list________________________")
    console.log(packages.join(" "));
    }

    list_packages();

    var widget = gui.currentPageWidget();

    widget.deselectAll();
	widget.selectComponent("qt.57.win32_mingw53");
	widget.selectComponent("qt.tools.win32_mingw530");
	widget.selectComponent("qt.57.qtcharts");
    gui.clickButton(buttons.NextButton,3000);
}

Controller.prototype.LicenseAgreementPageCallback = function() {
    gui.currentPageWidget().AcceptLicenseRadioButton.setChecked(true);
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.StartMenuDirectoryPageCallback = function() {
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.ReadyForInstallationPageCallback = function()
{
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.FinishedPageCallback = function() {
var checkBoxForm = gui.currentPageWidget().LaunchQtCreatorCheckBoxForm;
if (checkBoxForm && checkBoxForm.launchQtCreatorCheckBox) {
    checkBoxForm.launchQtCreatorCheckBox.checked = false;
}
    gui.clickButton(buttons.FinishButton,3000);
}
