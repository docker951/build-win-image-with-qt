FROM mcr.microsoft.com/windows/servercore:1909-amd64
LABEL maintainer="khotk.ilya@gmail.com"
WORKDIR C:\\maindir
COPY install_choco.ps1 C:\\maindir

RUN powershell -Command ".\install_choco.ps1; choco install -y git.install;"

############## Installing git behind proxy
#RUN powershell -Command " \
#	$env:chocolateyProxyLocation = 'http://<PROXY>:<PORT>';\
#	$env:chocolateyProxyUser = '<PROXY_USER>';\
#	$env:chocolateyProxyPassword = '<PROXY_PASSWORD>';\
#	.\install_choco.ps1;\
#	choco config set proxy http://<PROXY>:<PORT>;\
#	choco config set proxyUser <PROXY_USER>;\
#	choco config set proxyPassword <PROXY_PASSWORD>;\
#	choco install -y git.install;\
#"

########5.7.1##########
COPY install5.7.qs C:\\maindir
COPY qt-opensource-windows-x86-mingw530-5.7.1.exe C:\\maindir
RUN powershell -Command ".\qt-opensource-windows-x86-mingw530-5.7.1.exe -v --script install5.7.qs"
########5.12.3##########
COPY install5.12.qs C:\\maindir
COPY qt-opensource-windows-x86-5.12.3.exe C:\\maindir
RUN powershell -Command ".\qt-opensource-windows-x86-5.12.3.exe --no-proxy -v --script install5.12.qs"

RUN del C:\maindir\*.exe; del C:\maindir\*.qs; del C:\maindir\*.ps1; powershell -Command "Write-Host \"END\""